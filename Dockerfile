FROM tensorflow/tensorflow

# Copy example scripts and training data https://codelabs.developers.google.com/codelabs/tensorflow-for-poets/
COPY tensorflow-for-poets-2 /tensorflow-for-poets-2

# Install tfjs converter
RUN pip install tensorflowjs

# Script converting frozen to saved model format (Source:https://github.com/tensorflow/tfjs/issues/85)
COPY convert_model.py /

# Install wget (used to get training data from webserver)
RUN apt-get update \
  && apt-get install -y wget

# Start retraining and conversion on startup of dockercontainer
COPY docker-entrypoint.sh /
RUN ["chmod", "+x", "/docker-entrypoint.sh"]
ENTRYPOINT ["/docker-entrypoint.sh"]

WORKDIR /