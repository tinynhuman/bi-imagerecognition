#!/bin/bash

#data import
#wget "http://141.57.8.88/~tneuman2/pic-app/export.php?categorySelect[]=1&submit=" -O /tensorflow-for-poets-2/tf_files/training-data.zip
wget "http://pic-app/pic-app/export.php?categorySelect[]=1&submit=" -O /tensorflow-for-poets-2/tf_files/training-data.zip
unzip /tensorflow-for-poets-2/tf_files/training-data.zip -d /tensorflow-for-poets-2/tf_files/

#IMAGE_DIR="/tensorflow-for-poets-2/tf_files/flower_photos"
IMAGE_DIR="/tensorflow-for-poets-2/tf_files/training-data"
IMAGE_SIZE=224
ARCHITECTURE="mobilenet_1.0_${IMAGE_SIZE}"
STEPS=500

#start retraining
python /tensorflow-for-poets-2/scripts/retrain.py  \
  --bottleneck_dir=/tensorflow-for-poets-2/tf_files/bottlenecks \
  --how_many_training_steps=${STEPS} \
  --model_dir=/tensorflow-for-poets-2/tf_files/models/ \
  --summaries_dir=/tensorflow-for-poets-2/tf_files/training_summaries/"${ARCHITECTURE}" \
  --output_graph=/tensorflow-for-poets-2/tf_files/retrained_graph.pb \
  --output_labels=/tensorflow-for-poets-2/tf_files/retrained_labels.txt \
  --architecture="${ARCHITECTURE}" \
  --image_dir="${IMAGE_DIR}"

#use retrained model on sample image: /tensorflow-for-poets-2/tf_files/flower_photos/daisy/21652746_cc379e0eea_m.jpg
cd /tensorflow-for-poets-2
python /tensorflow-for-poets-2/scripts/label_image.py \
    --graph=/tensorflow-for-poets-2/tf_files/retrained_graph.pb  \
    --image=/tensorflow-for-poets-2/tf_files/flower_photos/daisy/21652746_cc379e0eea_m.jpg
cd /

#convert retrain.py output to saved model format
python /convert_model.py

#convert saved model to web-friendly format
tensorflowjs_converter /savedmodel /converted \
    --input_format=tf_saved_model \
    --output_node_names='final_result' \
    --saved_model_tags=serve

#compress converted model
cp /tensorflow-for-poets-2/tf_files/retrained_labels.txt /converted/
tar -cvzf /converted.tar.gz /converted/

#View training summaries in Tensorboard
tensorboard --logdir /tensorflow-for-poets-2/tf_files/training_summaries