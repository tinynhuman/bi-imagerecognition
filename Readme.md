# Option A: retrain inside single Docker container
This part explains how to run retrain pipeline without the 2nd Docker container(PHP web server)
In docker-entypoint.sh either change wget url to http://141.57.8.88/~tneuman2/pic-app/.. to get training data from external server 
or set IMAGE_DIR variable to /tensorflow-for-poets-2/tf_files/flower_photos to use examples from tutorial

1. build image

        $ docker build . -t biir

2. run container with Tensorboard on http://localhost:6006

        $ docker run --name biircontainer -p 6006:6006 biir

3. copie retrained converted model to MMDB Project on Host

        $ docker cp biircontainer:/converted.tar.gz /home/tina/Project/mmdb-fotoapp/pic-app/tfjs/converted-models

## Tensorflow for Poets Tutorial & example training data
Tutorial can be found here: https://www.tensorflow.org/tutorials/image_retraining \
To retrain using Tutorials example images, curl the files and copie to tensorflow-for-poets-2/tf_files/flower_photos (in tensorflow container):

    $ curl http://download.tensorflow.org/example_images/flower_photos.tgz \
            | tar xz -C tf_files

# Option B: retrain using both Docker containers
1. build images

        $ docker-compose up --build

2. run containers (PHP app on http://localhost/pic-app and Tensorboard on http://localhost:6006)

        $ docker-compose up

3. if category names have changed copie contents from model_volume retrained_lables.txt to /var/www/html/pic-app/tfjs/mobilenet/retrained_classes.js on web server container and rebuild the typescript code on the web server with the folowing commands:

        $ docker exec -it biimagerecognition_pic-app_1 bash

        $ cd /var/www/html/pic-app/tfjs
    
        $ npm install
    
        $ npm run build

# Links & (re)sources

## TFjs & converter
https://github.com/tensorflow/tfjs-converter

https://github.com/tensorflow/tfjs-converter/tree/master/demo/mobilenet

https://github.com/tensorflow/tfjs/issues/85

## TensorFlow for Poets
https://www.tensorflow.org/tutorials/image_retraining

https://github.com/googlecodelabs/tensorflow-for-poets-2

https://codelabs.developers.google.com/codelabs/tensorflow-for-poets-2